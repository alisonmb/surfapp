export const environment = {
  production: false,
  api: 'http://localhost:80/api/',
  localStoragekey: 'surfai',
  lsReports: 'surfaiReports',
  version: '0.2.4'
};
