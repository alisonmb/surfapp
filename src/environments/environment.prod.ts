export const environment = {
  production: true,
  api: 'https://fair-theater-286500.rj.r.appspot.com/api',
  localStoragekey: 'surfai',
  lsReports: 'surfaiReports',
  version: '0.3.0'
};
