import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ListviewComponent } from './listview/listview.component';
import { MapviewComponent } from './mapview/mapview.component';
import { ReportFormComponent } from './report-form/report-form.component';
import { ReportComponent } from './report/report.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  { path: '',                component: HomeComponent },
  { path: 'lista',           component: ListviewComponent },
  { path: 'mapa',            component: MapviewComponent },
  { path: 'form',            component: ReportFormComponent },
  { path: 'login',           component: LoginComponent },
  { path: 'signup',          component: SignupComponent },
  { path: 'config',          component: SettingsComponent },
  { path: 'report/:id',      component: ReportComponent },
  { path: '**',              redirectTo: 'home' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
