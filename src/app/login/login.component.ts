import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { AppComponent } from '../app.component';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  invalidLogin = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private appComponent: AppComponent,
              private toasterService: ToasterService) {
    this.createForm();
    this.toasterService = toasterService;
  }

  createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.authService.isAuthenticated().then((authenticated) => {
      this.invalidLogin = !authenticated;
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    const username = this.loginForm.get('username').value;
    const password = this.loginForm.get('password').value;

    this.authService.login(username, password).subscribe(
      (res: any) => {
          this.authService.setToken(res.body.id, res.body.userId);
          this.router.navigate(['form']);
          this.appComponent.loggedUser = this.authService.getUserId();
        },
        err => {
          let msg = '';
          (err.status === 401) ? msg = 'Email/Senha incorreto' : msg = err.message;
          this.toasterService.pop('error', 'Erro', msg);
          this.invalidLogin = true;
          this.loginForm.reset();
          console.log('err', err);
        }
      );
  }
}
