import {ActivatedRoute, Router} from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Settings } from '../settings';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContainerFileService } from '../services/container-file.service';
import { ReportService } from '../services/report.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToasterModule, ToasterService, BodyOutputType } from 'angular2-toaster';
import { environment } from '../../environments/environment';
import { AuthService } from '../services/auth.service';
import * as moment from 'moment';

@Component({
  selector: 'app-report-form',
  templateUrl: './report-form.component.html',
  styleUrls: ['./report-form.component.scss']
})
export class ReportFormComponent implements OnInit {

  submitted = false;
  reportForm: FormGroup;
  loggedUser;
  files = [];
  previewFiles = [];
  filesS3Url = [];
  wave_sizes        = Settings.wave_sizes;
  wave_directions   = Settings.wave_direction;
  wave_formations   = Settings.wave_formations;
  wave_conditions   = Settings.wave_conditions;
  wind_directions   = Settings.wind_directions;
  wind_intesities   = Settings.wind_intensities;
  waves_score       = Settings.wave_score;
  water_temperature = Settings.water_temperature;
  weather           = Settings.weather;
  sky               = Settings.sky;
  crowd             = Settings.crowd;
  states            = Settings.locations;
  cities            = [];
  spots;
  refreshIntervalId;
  loadingSave = false;
  imagesUploaded = [];


  constructor(
    private containerFileService: ContainerFileService,
    private _Activatedroute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private authService: AuthService,
    private reportService: ReportService,
    private toasterService: ToasterService) {
      this.toasterService = toasterService;
    }


  ngOnInit() {
    const report = this._Activatedroute.params['_value'];

    console.log(report);
    console.log(report.hasOwnProperty('id'));

    // Verifica se é edição
    if (this._Activatedroute.params['_value'].hasOwnProperty('id')) {

      // Verifica se tem estado setado
      if (report.state !== 'null') {
        this.selectedState(null, report.state);
      }

      // Verifica se tem imagens e seta as flags
      if (report.uploads.length) {
        // split por que esta tudo como uma stringona
        report.uploads.split(',').forEach((u) => {
          this.imagesUploaded.push(true);
          this.previewFiles.push(u);
        });
      }

    }

    this.authService.isAuthenticated().then((authenticated) => {
      if (!authenticated) {
        this.toasterService.pop({
          type: 'info', title: 'Você não está logado', showCloseButton: true, timeout: 9999,
          body: 'Seu report será enviado anonimamente, para participar das promoções você precisa fazer <a href="/login">login.</a>',
          bodyOutputType: BodyOutputType.TrustedHtml,
        });
      } else {
        this.loggedUser = this.authService.getUserId();
      }
    });

    this.reportForm = this.formBuilder
      .group({
        spot:           [report.spot, Validators.required],
        state:          [report.state],
        city:           [report.city],
        wind_direction: [report.wind_direction],
        wind_intensity: [report.wind_intensity],
        wave_size:      [report.wave_size],
        wave_direction: [report.wave_direction],
        wave_formation: [report.wave_formation],
        wave_condition: [report.wave_formation],
        wave_score:     [report.wave_score],
        comment:        [report.comment === 'null' ? '' : report.comment],
        id:             [report.hasOwnProperty('id') ? report.id : null],
        wt:             [report.wt],
        weather:        [report.weather],
        sky:            [report.sky],
        crowd:          [report.crowd],
        uploads:        [report.uploads]
      });

    this.reportService.getLocations();

    this.http.get(environment.api + 'spots/nearestSpots?' +
      'lat=' + this.reportService.location.lat +
      '&lng=' + this.reportService.location.lng
    ).subscribe((n) => {
      this.spots = n;
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.reportForm.controls;
  }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.reportForm.invalid) {
      console.log('this.reportForm (invalid)', this.reportForm);
      return;
    }

    this.loadingSave = true;

    // save url files
    this.reportForm.value.uploads = this.filesS3Url;
    this.reportForm.value.location = {lat: this.reportService.location.lat, lng: this.reportService.location.lng};
    this.reportForm.value.userId = (this.loggedUser) ? this.loggedUser.userId : null;

    if (this.reportForm.value.id) {
      this.editReport(this.reportForm.value);
    } else {
      this.saveReport(this.reportForm.value);
    }
  }

  saveReport(report) {
    this.http.post(environment.api + 'reports', report).subscribe(r => {
      console.log('report added:', r);
      this.toasterService.pop('success', 'Salvo', 'Report adicionado com sucesso');
      this.loadingSave = false;
      this.http.get(environment.api + 'reports/nearestReport?lat=' +
        this.reportService.location.lat + '&lng=' + this.reportService.location.lng)
        .subscribe(reports => {
          localStorage.setItem('surfaiReports', JSON.stringify({reports: reports}));
          this.reportService.reports = reports;
          this.router.navigate(['/lista']);
        });
    }, error => {
      // this.storageReportFailed(report);
      this.loadingSave = false;
      this.toasterService.pop('error', 'Erro', error.message);
      console.error('Error POST Report:', error.message);
    });
  }

  editReport(report) {
    console.log('editReport', report);
  }

  storageReportFailed(report) {
    const arrReportsFailed = this.getReportsFailed();

    // Verifica se o report atual já foi mandado (repetido)
    const index = arrReportsFailed.findIndex(r => r.spot === report.spot);
    if (index === -1) {
      arrReportsFailed.push(report);
    } else {
      arrReportsFailed[index] = report;
    }

    // Salva no localStorage
    localStorage.setItem('surfaiReportFailed', JSON.stringify(arrReportsFailed));

    this.refreshIntervalId = setInterval(this.trySave, 10000);
  }

  trySave() {

    // this.getReportsFailed;

    // const arrReportsFailed = this.getReportsFailed;
    // console.log('trySave() arrReportsFailed');
    //
    // // Se não estiver vazio
    // if (arrReportsFailed.length > 0) {
    //   arrReportsFailed.forEach(function (r) {
    //     console.log('r', r);
    //     clearInterval(this.refreshIntervalId);
    //   });
    // } else {
    //     clearInterval(this.refreshIntervalId);
    // }

  }


  getReportsFailed() {
    let arrReportsFailed = [];

    // Pega os reports failed no localStorage
    const surfaiReportFailed = localStorage.getItem( 'surfaiReportFailed');
    console.log('surfaiReportFailed', surfaiReportFailed);
    if (surfaiReportFailed) {
      arrReportsFailed = JSON.parse(surfaiReportFailed);
    }

    return arrReportsFailed;
  }


  onFileSelected(event) {

    // Controle loader do upload das imagens
    this.imagesUploaded.push(false);

    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = e => this.previewFiles.push(reader.result);

    this.files.push(file);

    reader.readAsDataURL(file);

    const formDataLogo = new FormData();

    const extension = (file.name).split('.')[1];

    let filename = moment().format('YYYYMMDD-hhmmss');
    filename = filename + '.' + extension;

    formDataLogo.append('uploadFile', file, filename);
    this.containerFileService.uploadFile('surfai', formDataLogo)
      .subscribe((result: any) => {
        this.filesS3Url.push(result.result.files.uploadFile[0].providerResponse.location);
        this.imagesUploaded[this.imagesUploaded.length - 1] = true;
        window.scrollTo(0, document.body.scrollHeight);
      }, error => {
        console.error('Error Upload file to S3:', error);
      });

  }

  selectedState(e, state) {
    let i;
    if (state) {
      i = this.states.findIndex(x => x.estado === state);
    } else {
      i = this.states.findIndex(x => x.estado === e.target.value);
    }

    if (i === 0) {
      this.cities = [];
    } else {
      this.cities = this.states[i].cidades;
    }
  }

  insertSpot(spot) {
    this.reportForm.patchValue({spot: spot});
  }

  removeFile(i) {
    this.previewFiles.splice(i, 1);
    this.filesS3Url.splice(i, 1);
    this.imagesUploaded.splice(i, 1);
  }

}
