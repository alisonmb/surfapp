import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ReportService } from '../services/report.service';

@Component({
  selector: 'app-listview',
  templateUrl: './listview.component.html',
  styleUrls: ['./listview.component.scss']
})
export class ListviewComponent implements OnInit {

  reports;

  constructor(private http: HttpClient, private reportService: ReportService) { }

  ngOnInit(): void {
    this.reports = this.reportService.reports === undefined ?
      JSON.parse(localStorage.getItem(environment.lsReports)).reports : this.reportService.reports;
    this.reportService.getReports();
  }

}
