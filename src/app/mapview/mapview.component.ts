import { Component, OnInit, ViewChild } from '@angular/core';
import { environment } from '../../environments/environment';
import { Settings } from '../settings';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ReportService } from '../services/report.service';

@Component({
  selector: 'app-mapview',
  templateUrl: './mapview.component.html',
  styleUrls: ['./mapview.component.scss']
})
export class MapviewComponent implements OnInit {

  @ViewChild('gmap') gmapElement: any;
  reports;
  userLat: number;
  userLng: number;
  coords = [];
  mapStyle;

  constructor(private router: Router,
              private http: HttpClient,
              private reportService: ReportService) {}

  ngOnInit() {

    this.mapStyle = Settings.mapStyle;

    this.reports = this.reportService.reports === undefined ?
      JSON.parse(localStorage.getItem(environment.lsReports)).reports : this.reportService.reports;
    this.reports.forEach((rep) => {
      this.coords.push({lat: rep.location.lat, lng: rep.location.lng});
    });

    console.log('navigator.geolocation', navigator.geolocation);
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        console.log('position', position);
        this.userLat = position.coords.latitude;
        this.userLng = position.coords.longitude;
      }, (err => {
        console.log('err', err);
      }));
    } else {
      alert('Geolocation is not supported by this browser.');
    }

    this.reportService.getReports();

    // Ajuste tamanho do mapa - Workaround para funcionar em mobiles devices
    document.getElementById('map-wrapper').style.height = (document.documentElement.clientHeight - 90) + 'px';
  }

  openReportDetail(i) {
    this.router.navigate(['/report', this.reports[i].id]);
  }

  ngAfterViewInit() {
    setTimeout(function () {
      document.getElementsByClassName('gmnoprint')[0]['style'].display = 'none';
      document.getElementsByClassName('gmnoprint')[1]['style'].display = 'none';
    }, 2000);


  }
}
