import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Settings } from '../settings';
import { ReportService } from '../services/report.service';
import * as moment from 'moment';
import {AuthService} from '../services/auth.service';
import {ToasterService} from 'angular2-toaster';
declare var $: any;

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  report;
  wave_sizes      = Settings.wave_sizes;
  wave_directions = Settings.wave_direction;
  wave_formations = Settings.wave_formations;
  wave_conditions = Settings.wave_conditions;
  wind_directions = Settings.wind_directions;
  wind_intensities = Settings.wind_intensities;
  wt = Settings.water_temperature;
  weather = Settings.weather;
  sky = Settings.sky;
  crowd = Settings.crowd;
  showEditButton = false;
  showDeleteButton = false;

  constructor(private _Activatedroute: ActivatedRoute,
              private http: HttpClient,
              private authService: AuthService,
              private toasterService: ToasterService,
              private router: Router,
              private reportService: ReportService) { }

  ngOnInit() {
    const params = this._Activatedroute.params['_value'];
    this.report = this.reportService.getReport(params.id);

    this.authService.isAuthenticated().then((authenticated) => {
      if (authenticated) {
        const user = this.authService.getUserId();
        if (this.report.userId === user.userId) {
          this.showEditButton = true;
          this.showDeleteButton = true;
        }
      }
    });
  }

  ngAfterViewInit() {
    $('#windy').attr('src', `https://embed.windy.com/embed2.html?lat=${this.report.location.lat}&lon=${this.report.location.lng}&zoom=9&level=surface&overlay=wind&menu=&message=true&marker=true&calendar=&pressure=&type=map&location=coordinates&detail=true&detailLat=${this.report.location.lat}&detailLon=${this.report.location.lng}&metricWind=km%2Fh&metricTemp=%C2%B0C&radarRange=-1`);
  }

  findTitleByKey(array, value) {
    for (let i = 0; i < array.length; i++) {
      if (array[i]['value'] === value) {
        return array[i].title;
      }
    }
    return null;
  }

  deleteReport(id) {
    const c = confirm('Tem certeza que deseja apagar seu boletim?');
    if (c === true) {
      this.http.delete(environment.api + 'reports/' + id).subscribe((r) => {
        console.log('data', r);
        if (r['count'] && r['count'] === 1) {
          this.toasterService.pop('success', 'Deletado', 'Boletim deletado com sucesso.');
          this.http.get(environment.api + 'reports/nearestReport?lat=' +
            this.reportService.location.lat + '&lng=' + this.reportService.location.lng)
            .subscribe(reports => {
              localStorage.setItem('surfaiReports', JSON.stringify({reports: reports}));
              this.reportService.reports = reports;
              this.router.navigate(['/lista']);
            });
        }
      });
    }
  }
}
