import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  loggedUser;
  version;

  constructor(private router: Router,
              private authService: AuthService) {
    this.loggedUser = authService.getUserId();
    this.version = environment.version;
  }

  ngOnInit() {
  }

  logout() {
    this.authService.removeLocalStorage();
    this.loggedUser = this.authService.getUserId();
  }
}
