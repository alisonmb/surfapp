export const Settings =  {
  wave_sizes: [
    {value: 0, title: 'Flat'},
    {value: 0.25, title: '0,5m-'},
    {value: 0.5, title: '0,5m'},
    {value: 0.75, title: '0,5m+'},
    {value: 1, title: '1m'},
    {value: 1.5, title: '1,5m'},
    {value: 2, title: '2m'},
    {value: 3, title: '3m+'},
  ],
  wave_direction: [
    {value: 0, title: 'N'},
    {value: 45, title: 'NE'},
    {value: 90, title: 'E'},
    {value: 135, title: 'SE'},
    {value: 180, title: 'S'},
    {value: 225, title: 'SO'},
    {value: 270, title: 'O'},
    {value: 315, title: 'NO'},
  ],
  wave_formations: [
    {value: 0, title: 'Mexida'},
    {value: 1, title: 'Fechando'},
    {value: 2, title: 'Cavada'},
    {value: 3, title: 'Cheia'},
    {value: 4, title: 'Regular'},
    {value: 5, title: 'Boa'}
  ],
  wave_conditions: [
    {value: 0, title: 'Ruim'},
    {value: 1, title: 'Regular'},
    {value: 2, title: 'Boa'},
    {value: 3, title: 'Excelente'}
  ],
  wind_intensities: [
    {value: 0, title: 'Forte'},
    {value: 1, title: 'Moderado'},
    {value: 2, title: 'Fraco'},
    {value: 3, title: 'Sem Vento'}
  ],
  wind_directions: [
    {value: 0, title: 'N'},
    {value: 1, title: 'NE'},
    {value: 2, title: 'E'},
    {value: 3, title: 'SE'},
    {value: 4, title: 'S'},
    {value: 5, title: 'SO'},
    {value: 6, title: 'O'},
    {value: 7, title: 'NO'}
  ],
  water_temperature: [
    {value: 0, title: 'Quente'},
    {value: 1, title: 'Ambiente'},
    {value: 2, title: 'Fria'},
    {value: 3, title: 'Gelada'}
  ],
  weather: [
    {value: 0, title: 'Quente'},
    {value: 1, title: 'Agradável'},
    {value: 2, title: 'Frio'},
    {value: 3, title: 'Seco'}
  ],
  sky: [
    {value: 0, title: 'Chuva'},
    {value: 1, title: 'Garoa'},
    {value: 2, title: 'Nublado'},
    {value: 3, title: 'Ensolarado'}
  ],
  crowd: [
    {value: 0, title: 'Sem Crowd'},
    {value: 1, title: 'Pequeno'},
    {value: 2, title: 'Moderado'},
    {value: 3, title: 'Intenso'}
  ],
  wave_score: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],

  locations: [
    { estado: '--- Outro ---', cidades: []},
    { estado: 'RS',
      cidades: [
        'Arroio do Sal',
        'Balneário Pinhal',
        'Capão da Canoa',
        'Cidreira',
        'Imbé',
        'Mostardas',
        'Osório',
        'Palmares do Sul',
        'Rio Grande',
        'Santa Vitória do Palmar',
        'São José do Norte',
        'Tavares',
        'Terra de Areia',
        'Torres',
        'Tramandaí',
        'Xangri-lá'
      ]
    },
    { estado: 'SC',
      cidades: [
        'Florianópolis',
        'Araranguá',
        'Baln Arroio do Silva',
        'Balneário Barra do Sul',
        'Balneário Camboriú',
        'Balneário Gaivota',
        'Balneário Piçarras',
        'Balneário Rincão',
        'Barra Velha',
        'Bombinhas',
        'Garopaba',
        'Governador Celso Ramos',
        'Imbituba',
        'Itajaí',
        'Itapema',
        'Itapoá',
        'Jaguaruna',
        'Laguna',
        'Navegantes',
        'Palhoça',
        'Passo de Torres',
        'Paulo Lopes',
        'Penha',
        'Porto Belo',
        'São Francisco do Sul',
        'Tijucas'
      ]
    },
    {
      estado: 'PR',
      cidades: [
        'Guaraqueçaba',
        'Guaratuba',
        'Matinhos',
        'Paranaguá',
        'Pontal do Paraná'
      ]
    },
    {
      estado: 'SP',
      cidades: [
        'ILHABELA',
        'Bertioga',
        'Cananéia',
        'Caraguatatuba',
        'Guarujá',
        'Iguape',
        'Ilha Comprida',
        'Itanhaém',
        'Mongaguá',
        'Peruíbe',
        'Praia Grande',
        'Santos',
        'São Sebastião',
        'São Vicente',
        'Ubatuba',
      ]
    },
    {
      estado: 'RJ',
      cidades: [
        'RIO DE JANEIRO',
        'Angra dos Reis',
        'Araruama',
        'Arm dos Búzios',
        'Arraial do Cabo',
        'Cabo Frio',
        'Campos dos Goytacazes',
        'Carapebus',
        'Casimiro de Abreu',
        'Ilha Grande',
        'Macaé',
        'Mangaratiba',
        'Maricá',
        'Niterói',
        'Parati',
        'Quissamã',
        'Rio das Ostras',
        'São Francisco de Itabapoana',
        'São João da Barra'
      ]
    },
    {
      estado: 'ES',
      cidades: [
        'VITÓRIA',
        'Anchieta',
        'Aracruz',
        'Conceição da Barra',
        'Fundão',
        'Guarapari',
        'Itapemirim',
        'Linhares',
        'Marataízes',
        'Piúma',
        'Presidente Kennedy',
        'São Mateus',
        'Serra',
        'Vila Velha'
      ]
    },
    {
      estado: 'BA',
      cidades: [
        'SALVADOR',
        'Alcobaça',
        'Belmonte',
        'Cairu',
        'Camaçari',
        'Canavieiras',
        'Caravelas',
        'Conde',
        'Entre Rios',
        'Esplanada',
        'Igrapiúna',
        'Ilhéus',
        'Itacaré',
        'Ituberá',
        'Jaguaripe',
        'Jandaíra',
        'Lauro de Freitas',
        'Maraú',
        'Mata de São João',
        'Mucuri',
        'Nilo Peçanha',
        'Nova Viçosa',
        'Porto Seguro',
        'Prado',
        'Santa Cruz Cabrália',
        'Una',
        'Uruçuca',
        'Valença',
        'Vera Cruz'
      ]
    },
    {
      estado: 'SE',
      cidades: [
        'ARACAJU',
        'Barra dos Coqueiros',
        'Brejo Grande',
        'Estância',
        'Itaporanga d`Ajuda',
        'Pacatuba',
        'Pirambu'
      ]
    },
    {
      estado: 'AL',
      cidades: [
        'MACEIÓ',
        'Barra de Santo Antônio',
        'Barra de São Miguel',
        'Coruripe',
        'Feliz Deserto',
        'Japaratinga',
        'Jequiá da Praia',
        'Maragogi',
        'Marechal Deodoro',
        'Paripueira',
        'Passo de Camaragibe',
        'Piaçabuçu',
        'Porto de Pedras',
        'São Miguel dos Milagres'
      ]
    },
    {
      estado: 'PE',
      cidades: [
        'RECIFE',
        'Barreiros',
        'Cabo de Santo Agostinho',
        'Goiana',
        'Ilha de Itamaracá',
        'Ipojuca',
        'Jaboatão dos Guararapes',
        'Olinda',
        'Paulista',
        'São José da Coroa Grande',
        'Sirinhaém',
        'Tamandaré'
      ]
    },
    {
      estado: 'PB',
      cidades: [
        'JOÃO PESSOA',
        'Baía da Traição',
        'Cabedelo',
        'Conde',
        'Lucena',
        'Mataraca',
        'Pitimbu',
        'Rio Tinto'
      ]
    },

    {
      estado: 'RN',
      cidades: [
        'NATAL',
        'Areia Branca',
        'Baía Formosa',
        'Caiçara do Norte',
        'Canguaretama',
        'Ceará-Mirim',
        'Extremoz',
        'Galinhos',
        'Grossos',
        'Guamaré',
        'Macau',
        'Maxaranguape',
        'Nísia Floresta',
        'Parnamirim',
        'Pedra Grande',
        'Porto do Mangue',
        'Rio do Fogo',
        'São Bento do Norte',
        'São Miguel do Gostoso',
        'Senador Georgino Avelino',
        'Tibau',
        'Tibau do Sul',
        'Touros'
      ]
    },
    {
      estado: 'CE',
      cidades: [
        'FORTALEZA',
        'Acaraú',
        'Amontada',
        'Aquiraz',
        'Aracati',
        'Barroquinha',
        'Beberibe',
        'Camocim',
        'Cascavel',
        'Caucaia',
        'Cruz',
        'Fortim',
        'Icapuí',
        'Itapipoca',
        'Itarema',
        'Jijoca de Jericoacoara',
        'Paracuru',
        'Paraipaba',
        'São Gonçalo do Amarante',
        'Trairi'
      ]
    },
    {
      estado: 'PI',
      cidades: [
        'Cajueiro da Praia',
        'Ilha Grande',
        'Luís Correia',
        'Parnaíba'
      ]
    },
    {
      estado: 'MA',
      cidades: [
        'SÃO LUÍS',
        'Alcântara',
        'Apicum-Açu',
        'Araioses',
        'Bacuri',
        'Barreirinhas',
        'Cândido Mendes',
        'Carutapera',
        'Cedral',
        'Cururupu',
        'Godofredo Viana',
        'Guimarães',
        'Humberto de Campos',
        'Icatu',
        'Luís Domingues',
        'Paulino Neves',
        'Porto Rico do Maranhão',
        'Primeira Cruz',
        'Raposa',
        'Santo Amaro do Maranhão',
        'São José de Ribamar',
        'Turiaçu',
        'Tutóia'
      ]
    },
    {
      estado: 'PA',
      cidades: [
        'Algodoal',
        'Bragança',
        'Marapanim',
        'Salinópolis',
        'Mosqueiro',
        'São João de Pirabas',
        'Soure'
      ]
    }
  ],

  mapStyle: [
    {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
    {elementType: 'labels.text.stroke', stylers: [{color: '#778da4'}]},
    {elementType: 'labels.text.fill', stylers: [{color: '#171b21'}]},
    {
      featureType: 'administrative.locality',
      elementType: 'labels.text.fill',
      stylers: [{color: '#fff'}]
    },
    {
      featureType: 'poi',
      elementType: 'labels.text.fill',
      stylers: [{color: '#171b21'}]
    },
    {
      featureType: 'poi.park',
      elementType: 'geometry',
      stylers: [{color: '#263c3f'}]
    },
    {
      featureType: 'road',
      elementType: 'geometry',
      stylers: [{color: '#38414e'}]
    },
    {
      featureType: 'road',
      elementType: 'geometry.stroke',
      stylers: [{color: '#212a37'}]
    },
    {
      featureType: 'road',
      elementType: 'labels.text.fill',
      stylers: [{visibility: 'off'}]
    },
    {
      featureType: 'road.highway',
      elementType: 'geometry',
      stylers: [{color: '#38414e'}]
    },
    {
      featureType: 'road.highway',
      elementType: 'geometry.stroke',
      stylers: [{color: '#1f2835'}]
    },
    {
      featureType: 'road.highway',
      elementType: 'labels',
      stylers: [{'visibility': 'off'}]
    },
    {
      featureType: 'transit',
      elementType: 'geometry',
      stylers: [{color: '#883600'}]
    },
    {
      featureType: 'transit.station',
      elementType: 'labels.text.fill',
      stylers: [{color: '#d59563'}]
    },
    {
      featureType: 'water',
      elementType: 'geometry',
      stylers: [{color: '#002d63'}]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.fill',
      stylers: [{color: '#515c6d'}]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.stroke',
      stylers: [{color: '#17263c'}]
    }
  ]
};
