import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { ReportService } from './services/report.service';
import { SwUpdate } from '@angular/service-worker';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  loggedUser;
  location;
  loadingUpdate = false;

  constructor(private authService: AuthService,
              private reportService: ReportService,
              private swUpdate: SwUpdate,
              private router: Router) {
    this.location = router;
    this.loggedUser = authService.getUserId();
    reportService.getReports();
  }

  logout() {
    this.authService.removeLocalStorage();
    this.loggedUser = this.authService.getUserId();
  }

  update() {
    this.loadingUpdate = true;
    this.reportService.getReports();
    this.swUpdate.checkForUpdate();
    setTimeout(() => {
      this.loadingUpdate = false;
      location.reload();
    }, 2000);
  }

  ngAfterViewInit() {
    $(document).click(function (event) {
      const navbar = $('.navbar-collapse');
      const _opened = navbar.hasClass('in');
      if (_opened === true && !$(event.target).hasClass('navbar-toggle')) {
        navbar.collapse('hide');
      }
    });
  }
}
