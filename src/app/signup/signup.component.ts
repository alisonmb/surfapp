import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { ToasterService } from 'angular2-toaster';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private http: HttpClient,
              private toasterService: ToasterService) {
    this.createForm();
    this.toasterService = toasterService;
  }

  createForm() {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required],
      phoneNumber: [''],
      userName: ['']
    });
  }

  ngOnInit() {
    this.authService.isAuthenticated().then((authenticated) => {
      if (authenticated) {
        this.toasterService.pop('error', 'Erro', 'Você já está logado, por favor faço logout para acessar com outra conta');
      }
    });
  }

  get f() {
    return this.signupForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    const user = this.signupForm.value;

    if (this.signupForm.invalid) {
      return;
    }

    this.http.post(environment.api + 'Accounts', this.signupForm.value).subscribe(r => {
      console.log('Accounts added:', r);
      this.toasterService.pop('success', 'Salvo', 'Accounts adicionado com sucesso');

      this.authService.login(user['email'], user['password']).subscribe(
        (res: any) => {
          this.authService.setToken(res.body.id, res.body.userId);
          this.router.navigate(['lista']);
        },
        err => {
          let msg = '';
          (err.status === 401) ? msg = 'Email/Senha incorreto' : msg = err.message;
          this.toasterService.pop('error', 'Erro', msg);
          this.signupForm.reset();
        }
      );

    }, error => {
      if (error.status === 422) {
        this.toasterService.pop('error', 'Erro!', 'Email já existe. Faça login ou use outro email.');
      } else {
        this.toasterService.pop('error', 'Erro', error.message);
      }
    });
  }

}
