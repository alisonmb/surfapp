import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  reports;
  location = {lat: 0, lng: 0};

  constructor(private http: HttpClient) { }

  getLocations() {
    navigator.geolocation.getCurrentPosition((position) => {
      this.location.lat = position.coords.latitude;
      this.location.lng = position.coords.longitude;
    });
  }

  getReports() {
    this.http.get(environment.api + 'reports/nearestReport?lat=' + this.location.lat + '&lng=' + this.location.lng)
      .subscribe(r => {
        localStorage.setItem('surfaiReports', JSON.stringify({reports: r}));
        this.reports = r;
        console.log('r', r);
    }, error => {
      console.error('Error GET Reports:', error);
      this.reports = JSON.parse(localStorage.getItem(environment.lsReports)).reports;
    });
  }

  getReport(id) {
    const report = this.findByKey(this.reports, id);
    console.log('report', report);
    if (report) {
      return report;
    } else {
      this.http.get(environment.api + 'reports/' + id).subscribe(r => {
        return r;
      }, error => {
        console.error('Error GET Denounces:', error);
      });
    }
  }

  findByKey(array, value) {
    for (let i = 0; i < array.length; i++) {
      if (array[i]['id'] === value) {
        return array[i];
      }
    }
    return null;
  }

}
