import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

const KEY = environment.localStoragekey;

@Injectable({
  providedIn: 'root'
})


export class AuthService {

  url = environment.api;
  user;

  constructor(private http: HttpClient, private router: Router) {
  }

  login(email: string, password: string) {
    return this.http.post(
        this.url + 'Accounts/login',
        {email: email, password: password},
        {observe: 'response'}
      );
  }

  setToken(token, user) {
    localStorage.setItem(KEY, JSON.stringify({
      token: token,
      userId: user
    }));
  }

  getUserId() {
    if (localStorage.getItem(KEY)) {
      return JSON.parse(localStorage.getItem(KEY));
    }
    return;
  }

  setUser(id) {
    return this.http.get(this.url + 'Accounts/' + id);
  }

  getUser() {
    return this.user;
  }

  removeLocalStorage() {
    localStorage.removeItem(environment.localStoragekey);
  }

  // getRoleUserAccount(id) {
  //   return this.http.get(`${this.url}Accounts/${id}/userRoles`);
  // }

  isAuthenticated(): Promise<boolean> {
    return Promise.resolve(!!localStorage.getItem(KEY));
  }

}
