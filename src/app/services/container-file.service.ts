import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})

export class ContainerFileService {

  uri = environment.api;
  constructor(private http: HttpClient) {
  }
  // denouncefyDev = 'denouncefy-dev';


  // Add request
  uploadFile(file, container) {
    return this.http.post(`${this.uri}container-files/${file}/upload/`, container);
  }

  // Get By container and file Request
  getFile(container, fileName) {
    return this.http.get(`${this.uri}/container-files/${container}/download/${fileName}`, {
      responseType: 'blob'});
  }

  // Delete file Request
  deleteFile(container, fileName) {
    return this.http.delete(`${this.uri}/container-files/${container}/download/${fileName}`);
  }
}
