import { TestBed } from '@angular/core/testing';

import { ContainerFileService } from './container-file.service';

describe('ContainerFileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContainerFileService = TestBed.get(ContainerFileService);
    expect(service).toBeTruthy();
  });
});
