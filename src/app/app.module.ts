import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MapviewComponent } from './mapview/mapview.component';
import { ListviewComponent } from './listview/listview.component';
import { AppRoutingModule } from './app-routing.module';
import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { ReportFormComponent } from './report-form/report-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BarRatingModule } from 'ngx-bar-rating';
import { ReportComponent } from './report/report.component';
import { SlideshowModule } from 'ng-simple-slideshow';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { AuthService } from './services/auth.service';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { SettingsComponent } from './settings/settings.component';


@NgModule({
  declarations: [
    AppComponent,
    MapviewComponent,
    ListviewComponent,
    ReportFormComponent,
    ReportComponent,
    LoginComponent,
    SignupComponent,
    HomeComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyA1_idbO82vdNz7s-wasPySYfesEkRK3Po'}),
    AgmJsMarkerClustererModule,
    BarRatingModule,
    SlideshowModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    [BrowserAnimationsModule, ToasterModule.forRoot()],
    AngularFontAwesomeModule,
    ShareButtonsModule.forRoot()
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
